Travel API Client 
=================

Clone this repo and start it (on windows systems use the gradlew.bat file):

`./gradlew bootRun`

to list all tasks:

`./gradlew tasks`

To view the assignment (after starting the application) go to:

[http://localhost:9000/travel/index.html](http://localhost:9000/travel/index.html)

# Search for fares
Go to the page http://localhost:9000/travel/search, enter the origin and destination to get the fare.

# Dashboard
Go to the page http://localhost:9000/travel/dashboard for information about the searches.