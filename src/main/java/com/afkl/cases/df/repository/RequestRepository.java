package com.afkl.cases.df.repository;

import com.afkl.cases.df.model.Request;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * This interface is used to store the Request objects.
 */
public interface RequestRepository extends CrudRepository<Request, Long> {

    @Query(value = "select top 1 * from request order by response_time", nativeQuery = true)
    Request findByMinResponseTime();

    @Query(value = "select top 1 * from request order by response_time desc", nativeQuery = true)
    Request findByMaxResponseTime();

    @Query(value = "select avg(response_time) from request", nativeQuery = true)
    Long averageResponseTime();

    @Query(value = "select count(*) from request where status_code = 200", nativeQuery = true)
    Long totalRequestsForOk();

    @Query(value = "select count(*) from request where status_code BETWEEN 400 AND 499", nativeQuery = true)
    Long totalRequestsFor4xx();

    @Query(value = "select count(*) from request where status_code BETWEEN 500 AND 599", nativeQuery = true)
    Long totalRequestsFor5xx();
}
