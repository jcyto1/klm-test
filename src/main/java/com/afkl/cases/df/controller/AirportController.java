package com.afkl.cases.df.controller;

import com.afkl.cases.df.model.Airport;
import com.afkl.cases.df.service.AirportService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;

@Controller
@AllArgsConstructor
public class AirportController {

    private final AirportService airportService;

    /**
     * This method will search for the airports by the giving term and return a list of airports sorted by the name.
     *
     * @param term the search term
     * @return sorted list of airports
     * @throws IOException the IO exception
     */
    @RequestMapping("/ajaxSearch")
    public @ResponseBody
    List<Airport> ajaxSearch(@RequestParam final String term) throws IOException {
        final List<Airport> airports = airportService.searchAirport(term);
        airports.sort(Comparator.comparing(Airport::getName));
        return airports;
    }
}
