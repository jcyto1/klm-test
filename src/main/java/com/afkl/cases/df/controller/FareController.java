package com.afkl.cases.df.controller;

import com.afkl.cases.df.model.Airport;
import com.afkl.cases.df.model.Fare;
import com.afkl.cases.df.model.FareResult;
import com.afkl.cases.df.model.Search;
import com.afkl.cases.df.service.AirportService;
import com.afkl.cases.df.service.FareService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.CompletableFuture;

@Controller
@AllArgsConstructor
public class FareController {

    private static final Logger LOG = LoggerFactory.getLogger(FareController.class);
    private static final String ATTRIBUTE_SEARCH = "search";
    private static final String ATTRIBUTE_RESULT = "result";
    private static final String ATTRIBUTE_ERROR_MESSAGE = "errorMessage";
    private static final String PAGE_SEARCH = "search";
    private static final String PAGE_FARE_RESULT = "fare-result";

    private final AirportService airportService;
    private final FareService fareService;

    @RequestMapping("/search")
    public String search(final Model model) {
        model.addAttribute(ATTRIBUTE_SEARCH, new Search());
        return PAGE_SEARCH;
    }

    @RequestMapping("/checkFare")
    public String checkFare(@ModelAttribute final Search search, final Model model,
                            final HttpServletResponse response) {
        final String origin = search.getOrigin();
        final String destination = search.getDestination();
        try {
            final CompletableFuture<Airport> resultOrigin = airportService.getAirportDetails(origin);
            final CompletableFuture<Airport> resultDestination = airportService.getAirportDetails(destination);
            final CompletableFuture<Fare> resultFare = fareService.getFare(origin, destination);
            CompletableFuture.allOf(resultOrigin, resultDestination, resultFare).join();

            final FareResult fareResult = new FareResult();
            fareResult.setOrigin(resultOrigin.get());
            fareResult.setDestination(resultDestination.get());
            fareResult.setFare(resultFare.get());
            model.addAttribute(ATTRIBUTE_RESULT, fareResult);
        } catch (final Exception ex) {
            response.setStatus(500);
            model.addAttribute(ATTRIBUTE_ERROR_MESSAGE, "Something went wrong, please try again.");
            LOG.error(ex.getMessage());
            LOG.error("Could not finish search for: {} - {}", origin, destination);
            return PAGE_SEARCH;
        }
        LOG.info("Finished search for: {} - {}", origin, destination);
        return PAGE_FARE_RESULT;
    }
}
