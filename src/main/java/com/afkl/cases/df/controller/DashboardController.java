package com.afkl.cases.df.controller;

import com.afkl.cases.df.model.Request;
import com.afkl.cases.df.service.RequestService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@AllArgsConstructor
public class DashboardController {

    private static final String ATTRIBUTE_MAX_REQUEST = "maxRequest";
    private static final String ATTRIBUTE_MIN_REQUEST = "minRequest";
    private static final String ATTRIBUTE_TOTAL_REQUESTS = "totalRequests";
    private static final String ATTRIBUTE_AVERAGE_RESPONSE_TIME = "averageResponseTime";
    private static final String ATTRIBUTE_TOTAL_OK_REQUESTS = "totalOkRequests";
    private static final String ATTRIBUTE_TOTAL_4_XX_REQUESTS = "total4xxRequests";
    private static final String ATTRIBUTE_TOTAL_5_XX_REQUESTS = "total5xxRequests";
    private static final String PAGE_DASHBOARD = "dashboard";

    private final RequestService requestService;

    @RequestMapping("/requestData")
    public @ResponseBody
    List<Request> getRequestData() {
        return requestService.list();
    }

    @RequestMapping("/dashboard")
    public String dashboard(final Model model) {
        final Request maxRequest = requestService.findMaxResponseTime();
        final Request minRequest = requestService.findMinResponseTime();
        final Long averageResponseTime = requestService.getAverageResponseTime();
        final Long totalRequests = requestService.getTotalRequests();
        final Long totalOkRequests = requestService.getTotalOkRequests();
        final Long total4xxRequests = requestService.getTotal4xxRequests();
        final Long total5xxRequests = requestService.getTotal5xxRequests();

        model.addAttribute(ATTRIBUTE_MAX_REQUEST, maxRequest);
        model.addAttribute(ATTRIBUTE_MIN_REQUEST, minRequest);
        model.addAttribute(ATTRIBUTE_TOTAL_REQUESTS, totalRequests);
        model.addAttribute(ATTRIBUTE_AVERAGE_RESPONSE_TIME, averageResponseTime);
        model.addAttribute(ATTRIBUTE_TOTAL_OK_REQUESTS, totalOkRequests);
        model.addAttribute(ATTRIBUTE_TOTAL_4_XX_REQUESTS, total4xxRequests);
        model.addAttribute(ATTRIBUTE_TOTAL_5_XX_REQUESTS, total5xxRequests);
        return PAGE_DASHBOARD;
    }

}
