package com.afkl.cases.df.model;

import lombok.Data;

/**
 * This class represents the search for the {@code FareController}.
 */
@Data
public class Search {

    private String origin;
    private String destination;

}
