package com.afkl.cases.df.model;

import lombok.Data;

/**
 * This class represents the fare object returned from the call:
 * http://localhost:8080/fares/{origin}/{destination}
 */
@Data
public class Fare {

    private double amount;
    private String currency;
    private String origin;
    private String destination;

}
