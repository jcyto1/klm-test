package com.afkl.cases.df.model;

import lombok.Data;

/**
 * This class represents the airport object returned from the calls:
 * - http://localhost:8080/airports/{code}
 * - http://localhost:8080/airports/?term=XXX
 */
@Data
public class Airport {

    private String code;
    private String name;
    private String description;

}
