package com.afkl.cases.df.model;

import lombok.Data;

/**
 * This class represents the combined result of the fares REST service.
 */
@Data
public class FareResult {

    private Airport origin;
    private Airport destination;
    private Fare fare;

}
