package com.afkl.cases.df.model;

import lombok.Data;

import javax.persistence.*;

/**
 * This class stores the request information for dashboard overview purpose.
 */
@Data
@Entity
@Table(name = "request")
public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long responseTime;
    private int statusCode;
    private String searchTerms;
    private String requestUrl;

}
