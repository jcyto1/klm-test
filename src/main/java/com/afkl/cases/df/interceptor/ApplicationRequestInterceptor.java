package com.afkl.cases.df.interceptor;

import com.afkl.cases.df.model.Request;
import com.afkl.cases.df.service.RequestService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@RequiredArgsConstructor
public class ApplicationRequestInterceptor implements HandlerInterceptor {

    private static final String PARAMETER_ORIGIN = "origin";
    private static final String PARAMETER_DESTINATION = "destination";

    private final RequestService requestService;
    private long start;

    @Override
    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response,
                             final Object handler) {
        start = System.currentTimeMillis();
        return true;
    }

    @Override
    public void postHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler,
                           final ModelAndView modelAndView) {
    }

    @Override
    public void afterCompletion(final HttpServletRequest request, final HttpServletResponse response,
                                final Object handler, final Exception ex) {
        final long end = System.currentTimeMillis();
        final long totalTime = end - start;
        final int status = response.getStatus();

        final String origin = ServletRequestUtils.getStringParameter(request, PARAMETER_ORIGIN, "");
        final String destination = ServletRequestUtils.getStringParameter(request, PARAMETER_DESTINATION, "");
        final StringBuilder searchTerms = new StringBuilder();
        if (!StringUtils.isEmpty(origin) || !StringUtils.isEmpty(destination)) {
            searchTerms.append("origin=").append(origin).append("&destination=").append(destination);
        }

        final Request logRequest = new Request();
        logRequest.setResponseTime(totalTime);
        logRequest.setStatusCode(status);
        logRequest.setSearchTerms(searchTerms.toString());
        logRequest.setRequestUrl(request.getRequestURI());
        requestService.save(logRequest);
    }
}
