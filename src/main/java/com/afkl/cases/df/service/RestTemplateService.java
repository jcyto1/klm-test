package com.afkl.cases.df.service;

import com.afkl.cases.df.interceptor.HeaderRequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * This service will return the RestTemplate or the Authorized RestTemplate.
 */
@Service
public class RestTemplateService {

    private static final String AUTHORIZATION_HEADER = "Authorization";
    private static final String PREFIX_AUTHORIZATION_HEADER = "Bearer ";

    private final OAuth2Service oAuth2Service;
    private final RestTemplate restTemplate;

    @Autowired
    public RestTemplateService(final OAuth2Service oAuth2Service, final RestTemplateBuilder restTemplateBuilder) {
        this.oAuth2Service = oAuth2Service;
        this.restTemplate = restTemplateBuilder.build();
    }

    /**
     * This method will create a new RestTemplate object supporting the authorization with the obtained access token
     * from the {@link OAuth2Service}.
     *
     * @return the rest template object
     */
    /*package private*/ RestTemplate getAuthorizedRestTemplate() {
        final String accessToken = oAuth2Service.getAccessToken();
        final List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
        interceptors.add(new HeaderRequestInterceptor(AUTHORIZATION_HEADER, PREFIX_AUTHORIZATION_HEADER + accessToken));

        restTemplate.setInterceptors(interceptors);
        return restTemplate;
    }
}
