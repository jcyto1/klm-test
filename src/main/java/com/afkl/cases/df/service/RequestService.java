package com.afkl.cases.df.service;

import com.afkl.cases.df.model.Request;
import com.afkl.cases.df.repository.RequestRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * This service will only save the incoming request and return all the list.
 */
@Service
@AllArgsConstructor
public class RequestService {

    private final RequestRepository requestRepository;

    /**
     * This method will fetch all the requests and convert it to a list.
     *
     * @return list of requests
     */
    public List<Request> list() {
        final Iterable<Request> requests = requestRepository.findAll();
        final List<Request> requestsList = new ArrayList<>();
        requests.forEach(requestsList::add);
        return requestsList;
    }

    @Transactional
    public void save(final Request request) {
        requestRepository.save(request);
    }

    public Request findMaxResponseTime() {
        return requestRepository.findByMaxResponseTime();
    }

    public Request findMinResponseTime() {
        return requestRepository.findByMinResponseTime();
    }

    public Long getTotalRequests() {
        return requestRepository.count();
    }

    public Long getAverageResponseTime() {
        return requestRepository.averageResponseTime();
    }

    public Long getTotalOkRequests() {
        return requestRepository.totalRequestsForOk();
    }

    public Long getTotal4xxRequests() {
        return requestRepository.totalRequestsFor4xx();
    }

    public Long getTotal5xxRequests() {
        return requestRepository.totalRequestsFor5xx();
    }
}
