package com.afkl.cases.df.service;

import com.afkl.cases.df.model.Fare;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

/**
 * This service will get the fares using the airport codes.
 */
@Service
@RequiredArgsConstructor
public class FareService {

    private static final String URL_DIVIDER = "/";

    @Value("${klm.mock.fareEndpoint}")
    private String fareEndpoint;

    private final RestTemplateService restTemplateService;

    /**
     * This method is annotated with @Async, due to the fact that getting the fares is a time consuming matter.
     * That is why this method is set to asynchronous. The parameters are the aiport code of the origin and destination.
     * By returning a future Fare object, fetching the data could be joined together in the controller.
     *
     * @param origin      airport code of origin
     * @param destination airport code of destination
     * @return a future Fare object
     */
    @Async
    public CompletableFuture<Fare> getFare(final String origin, final String destination) {
        final String fareUrl = fareEndpoint + URL_DIVIDER + origin + URL_DIVIDER + destination;

        final RestTemplate restTemplate = this.restTemplateService.getAuthorizedRestTemplate();
        final Fare fare = restTemplate.getForObject(fareUrl, Fare.class);
        return CompletableFuture.completedFuture(fare);
    }
}
