package com.afkl.cases.df.service;

import com.afkl.cases.df.model.Airport;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * This service will get the airport details and search for airports.
 */
@Service
@RequiredArgsConstructor
public class AirportService {

    @Value("${klm.mock.airportEndpoint}")
    private String airportEndpoint;

    private final RestTemplateService restTemplateService;

    /**
     * This method is annotated with @Async, due to the fact that getting the details is a time consuming matter.
     * That is why this method is set to asynchronous. The airport will be fetched by using the airport code.
     * By returning a future Airport object, fetching the data could be joined together in the controller.
     *
     * @param code the airport code
     * @return a future Airport object
     */
    @Async
    public CompletableFuture<Airport> getAirportDetails(final String code) {
        final String airportUrl = airportEndpoint + code;

        final RestTemplate restTemplate = restTemplateService.getAuthorizedRestTemplate();
        final Airport airport = restTemplate.getForObject(airportUrl, Airport.class);
        return CompletableFuture.completedFuture(airport);
    }

    /**
     * This method will search for the airports by the given 'term'.
     *
     * @param term The search term
     * @return A list of Airports
     * @throws IOException the IO exception
     */
    public List<Airport> searchAirport(final String term) throws IOException {
        final String searchUrl = airportEndpoint + "?term=" + term;
        final RestTemplate restTemplate = restTemplateService.getAuthorizedRestTemplate();
        final ResponseEntity response = restTemplate.getForEntity(searchUrl, String.class);

        final JsonNode locations = getLocationsNode(response);
        return getListOfAirportsFromLocationsNode(locations);
    }

    private JsonNode getLocationsNode(final ResponseEntity response) throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        final JsonNode root = mapper.readTree(response.getBody().toString());
        final JsonNode embedded = root.get("_embedded");
        return embedded.get("locations");
    }

    private List<Airport> getListOfAirportsFromLocationsNode(final JsonNode locations) {
        final List<Airport> airports = new ArrayList<>();
        if (locations.isArray()) {
            for (final JsonNode locationNode : locations) {
                final Airport airport = new Airport();
                airport.setCode(locationNode.get("code").textValue());
                airport.setName(locationNode.get("name").textValue());
                airport.setDescription(locationNode.get("description").textValue());
                airports.add(airport);
            }
        }
        return airports;
    }
}
