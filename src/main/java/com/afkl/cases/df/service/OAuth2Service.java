package com.afkl.cases.df.service;

import com.afkl.cases.df.model.Token;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;

/**
 * This service provide the method to get the access token from the KLM mock project.
 * The access token will be used in the REST calls by other services.
 */
@Service
public class OAuth2Service {

    private static final String HEADER_AUTHORIZATION = "Authorization";
    private static final String AUTH_COLON = ":";
    private static final String ENCODING_FORMAT = "UTF-8";
    private static final String PREFIX_AUTHORIZATION_HEADER = "Basic ";

    @Value("${klm.mock.tokenEndpoint}")
    private String tokenEndpoint;

    @Value("${klm.mock.clientId}")
    private String clientId;

    @Value("${klm.mock.clientSecret}")
    private String clientSecret;

    private final RestTemplate restTemplate;

    public OAuth2Service(final RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    /**
     * This method will get the access token. At the moment, we do not support refresh token.
     * This means we will fetch the access token each time we make a REST call.
     * We also assume that the service will be available 24/7 and no downtime, no error catching.
     *
     * @return access token
     */
    /*package private*/ String getAccessToken() {
        final HttpHeaders authorizationHeaders = createAuthorizationHeaders();
        final HttpEntity<Object> entity = new HttpEntity<>(authorizationHeaders);
        final Token token = restTemplate.postForObject(tokenEndpoint, entity, Token.class);
        return token.getAccessToken();
    }

    /**
     * This method will create the HTTP headers 'basic authorization' using
     * the fields {@code clientId} and {@code clientSecret}.
     *
     * @return basic authorization headers
     */
    private HttpHeaders createAuthorizationHeaders() {
        final String auth = clientId + AUTH_COLON + clientSecret;
        final byte[] encodedAuth = Base64Utils.encode(auth.getBytes(Charset.forName(ENCODING_FORMAT)));
        final String authorizationHeader = PREFIX_AUTHORIZATION_HEADER + new String(encodedAuth);

        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(HEADER_AUTHORIZATION, authorizationHeader);
        return httpHeaders;
    }
}
