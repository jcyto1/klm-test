package com.afkl.cases.df.controller;

import com.afkl.cases.df.Bootstrap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = Bootstrap.class)
@AutoConfigureMockMvc
public class FareControllerIntegrationTest {

    @Test
    public void doSomeIntegrationTest() {
        // Due to time I could not finish the integration tests...
    }
}