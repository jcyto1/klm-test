package com.afkl.cases.df.service;

import com.afkl.cases.df.model.Fare;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
public class FareServiceTest {

    @InjectMocks
    private FareService fareService;

    @Mock
    private RestTemplateService mockRestTemplateService;

    @Mock
    private RestTemplate mockRestTemplate;

    @Test
    public void shouldGetFare() throws ExecutionException, InterruptedException {
        // given
        final double expectedAmount = 1234.56;
        final String expectedCurrency = "EUR";
        final String expectedOrigin = "AMS";
        final String expectedDestination = "HKG";
        final Fare expectedFare = new Fare();
        expectedFare.setAmount(expectedAmount);
        expectedFare.setCurrency(expectedCurrency);
        expectedFare.setOrigin(expectedOrigin);
        expectedFare.setDestination(expectedDestination);

        given(mockRestTemplate.getForObject(anyString(), any())).willReturn(expectedFare);
        given(mockRestTemplateService.getAuthorizedRestTemplate()).willReturn(mockRestTemplate);

        // when
        final CompletableFuture<Fare> actualResult = fareService.getFare(expectedOrigin, expectedDestination);

        // then
        assert actualResult != null;
        assert actualResult.get().equals(expectedFare);
    }
}