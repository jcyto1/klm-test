package com.afkl.cases.df.service;

import com.afkl.cases.df.interceptor.HeaderRequestInterceptor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class RestTemplateServiceTest {

    private RestTemplateService restTemplateService;

    @Mock
    private OAuth2Service mockOAuth2Service;

    @Before
    public void setUp() {
        restTemplateService = new RestTemplateService(mockOAuth2Service, new RestTemplateBuilder());
    }

    @Test
    public void shouldGetRestTemplate() {
        // given
        final String expectedToken = "token";

        // when
        when(mockOAuth2Service.getAccessToken()).thenReturn(expectedToken);

        final RestTemplate actualResult = restTemplateService.getAuthorizedRestTemplate();

        // then
        assert actualResult != null;
        assert actualResult.getInterceptors().size() == 1;
        assert actualResult.getInterceptors().get(0) instanceof HeaderRequestInterceptor;
    }
}