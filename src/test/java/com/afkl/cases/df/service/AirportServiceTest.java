package com.afkl.cases.df.service;

import com.afkl.cases.df.model.Airport;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class AirportServiceTest {

    @Mock
    private RestTemplateService restTemplateService;

    @Mock
    private RestTemplate mockRestTemplate;

    @InjectMocks
    private AirportService airportService;

    @Test
    public void shouldGetAirportDetails() throws ExecutionException, InterruptedException {
        // given
        final String code = "code";
        final Airport expectedAirport = new Airport();
        expectedAirport.setCode(code);
        expectedAirport.setName("airport name");
        expectedAirport.setDescription("airport destination");

        // when
        when(restTemplateService.getAuthorizedRestTemplate()).thenReturn(mockRestTemplate);
        when(mockRestTemplate.getForObject(anyString(), any())).thenReturn(expectedAirport);
        final CompletableFuture<Airport> actualResult = airportService.getAirportDetails(code);

        // then
        assert actualResult != null;
        assert actualResult.get().equals(expectedAirport);
    }

    @Test
    public void shouldSearchAirport() throws IOException {
        // given
        final String term = "term";
        final String expectedJson = "{" +
                "    \"_embedded\": {" +
                "        \"locations\": [" +
                "            {" +
                "                \"code\": \"HNL\"," +
                "                \"name\": \"Honolulu Intl.\"," +
                "                \"description\": \"Honolulu - Honolulu Intl. (HNL), USA\"," +
                "                \"coordinates\": {" +
                "                    \"latitude\": 21.32583," +
                "                    \"longitude\": -157.92167" +
                "                }" +
                "            }," +
                "            {" +
                "                \"code\": \"AMS\"," +
                "                \"name\": \"Schiphol\"," +
                "                \"description\": \"Amsterdam - Schiphol (AMS), Netherlands\"," +
                "                \"coordinates\": {" +
                "                    \"latitude\": 52.30833," +
                "                    \"longitude\": 4.76806" +
                "                }" +
                "            }" +
                "          ]" +
                "    }" +
                "}";
        final ResponseEntity responseEntity = new ResponseEntity<>(expectedJson, HttpStatus.OK);

        // when
        when(restTemplateService.getAuthorizedRestTemplate()).thenReturn(mockRestTemplate);
        when(mockRestTemplate.getForEntity(anyString(), any())).thenReturn(responseEntity);
        final List<Airport> actualResult = airportService.searchAirport(term);

        // then
        assert actualResult != null;
        assert actualResult.size() == 2;
    }
}