package com.afkl.cases.df.service;

import com.afkl.cases.df.model.Token;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@RestClientTest(OAuth2Service.class)
public class OAuth2ServiceTest {

    @Autowired
    private OAuth2Service oAuth2Service;

    @Autowired
    private MockRestServiceServer mockServer;

    @Autowired
    private ObjectMapper objectMapper;

    private final String expectedAccessToken = "access-token";

    @Before
    public void setUp() throws Exception {
        final Token expectedToken = new Token();
        expectedToken.setAccessToken(expectedAccessToken);

        final String detailsString =
                objectMapper.writeValueAsString(expectedToken);

        this.mockServer.expect(requestTo("http://localhost:8080/oauth/token?grant_type=client_credentials"))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withSuccess(detailsString, MediaType.APPLICATION_JSON));
    }

    @Test
    public void shouldGetAccessToken() {
        // when
        final String actualResult = oAuth2Service.getAccessToken();

        // then
        assertNotNull(actualResult);
        assertEquals(actualResult, expectedAccessToken);
    }
}